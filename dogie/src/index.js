import React from 'react';
import ReactDOM from 'react-dom';
import './App.css';
import * as serviceWorker from './serviceWorker';
import Routes from './Routes';
import {Router} from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import Grid from '@material-ui/core/Grid';
const history = createHistory();
ReactDOM.render(
    <Router history={history}>

    <Grid container spacing={3}>
        <Grid item xs={12}>
            <Routes/>
        </Grid>
    </Grid>

</Router>, document.getElementById('root'));

serviceWorker.unregister();
