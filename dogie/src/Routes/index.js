import React from 'react';
import {Switch, Route} from 'react-router-dom';
import SignIn from '../Components/TextFieldEle';
import Signup from '../Components/Signup';
import Home from '../Components/List';
import Detail from '../Components/Detail';
import {Router} from 'react-router-dom';
import history from '../services/history';
import NotFound from '../Components/NotFound';
import PrivateRoute from '../Routes/PrivateRoute';
import PublicRoute from '../Routes/PublicRoute';
function Routes() {
    return (
        <Router history={history}>

            <Switch>
                <PublicRoute restricted={true} exact path="/" component={SignIn}/>

                <PublicRoute restricted={true} path="/Register" component={Signup}/>
                <PrivateRoute exact path="/Home" component={Home}/>
                <PrivateRoute path="/Breed" component={Detail}/>
                <Route component={NotFound}/>
            </Switch>
        </Router>
    );

}

export default Routes;