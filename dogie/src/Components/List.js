import React, {useState, useEffect} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import '../App.css';
import CardView from './CardView';
import LinearProgress from '@material-ui/core/LinearProgress';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import history from '../services/history';
import {logout} from '../utils/index';
const useStyles = makeStyles({
    title: {
        flexGrow: 1
    }
});
function List() {

    const [error,
        setError] = useState(null);
    const [isLoaded,
        setIsLoaded] = useState(false);
    const [breed,
        setbreed] = useState([]);
    const classes = useStyles();
    useEffect(() => {
        fetch("https://dog.ceo/api/breeds/list/all")
            .then(res => res.json())
            .then((result) => {
                setIsLoaded(true);
                setbreed(Object.keys(result.message));


            },

            (error) => {
                setIsLoaded(true);
                setError(error);
            })
    }, [])
    if (breed.length !== 0) {

    }
    function handleLogout() {
        logout();
        history.push("/");
    }
    if (error) {
        return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
        return <LinearProgress variant="buffer" value={'completed'} valueBuffer={'buffer'}/>;
    } else {
        return (
            <div>
                <AppBar position="static">
                    <Toolbar>

                        <Typography variant="h6" className={classes.title}>
                            Breed List
                        </Typography>
                        <Button className="Button" onClick={handleLogout} color="inherit">Logout</Button>
                    </Toolbar>

                </AppBar>
                {breed.map((value, index) => {
                    return <CardView key={index} value={value}/>;
                })}
            </div>

        );
    }
}
export default List;