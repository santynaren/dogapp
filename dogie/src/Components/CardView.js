import React, {useState, useEffect} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CircularProgress from '@material-ui/core/CircularProgress';
import '../App.css';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardActionArea from '@material-ui/core/CardActionArea';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {Link} from 'react-router-dom';

const useStyles = makeStyles({
    root: {

        textAlign: "left",

        margin: 30,
        alignContent: "center",
        justifyContent: "center"

    },
    TextInput: {
        borderColor: "#0099d9",
        margin: 15
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)'
    },
    title: {
        fontSize: 14
    },
    pos: {
        marginBottom: 12
    },
    ButtonDiv: {
        flexDirection: "columns"
    },
    ButtonEle: {
        margin: 15
    },
    mainDiv: {
        display: 'flex',
        flexDirection: "columns"
    },
    subDiv: {
        display: 'flex',
        flexDirection: "rows"
    }

});

function CardView(props) {

    const items = [];
    const [imgg,
        setImg] = useState('');
    const classes = useStyles();
    const [isLoaded,
        setIsLoaded] = useState(false);
    const [error,
        setError] = useState(null);

    useEffect(() => {
        fetch("https://dog.ceo/api/breed/" + props.value + "/images/random")
            .then(res => res.json())
            .then((result) => {
                setIsLoaded(true);
                setImg((result.message));
                console.log(result.message);

            }, (error) => {
                setIsLoaded(true);
                setError(error);
                console.log("error");
            })
    }, [])

    if (error) {
        return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
        items.push(<CircularProgress/>);
    } else {
        items.push(<img src={imgg} alt="breed_images" className="App_logo"/>);
    }

    return (

        <Card className={classes.root}>
            <CardActionArea >
                <div className={classes.mainDiv}>
                    {items
}

                    <div className={classes.subDiv}>
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="h2">
                                {props.value}
                            </Typography>
                            <Typography variant="body2" color="textSecondary" component="p">
                                Extracted from Dog API
                            </Typography>
                            <CardActions>
                                <Link
                                    to={{
                                    pathname: '/Breed',
                                    state: {
                                        val: props.value
                                    }
                                }}>
                                    <Button size="small" color="primary">
                                        Know More
                                    </Button>
                                </Link>

                            </CardActions>
                        </CardContent>
                    </div>

                </div>

            </CardActionArea>

        </Card>

    );

}
export default CardView;