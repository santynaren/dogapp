import React, {useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import '../App.css';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import CardMedia from '@material-ui/core/CardMedia';
import history from '../services/history';

import Grid from '@material-ui/core/Grid';
import "firebase/firestore";
import firebaseapp from '../Firebase';
import {login} from '../utils/index';
import Snackbar from '@material-ui/core/Snackbar';

const useStyles = makeStyles({
    root: {
        minWidth: 275,
        textAlign: "left",
        width: 300,
        alignContent: "center",
        justifyContent: "center"

    },
    TextInput: {
        borderColor: "#0099d9",
        margin: 15
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)'
    },
    title: {
        fontSize: 14
    },
    pos: {
        marginBottom: 12
    },
    ButtonDiv: {
        flexDirection: "columns"
    },
    ButtonEle: {
        margin: 15
    }

});

function TextFieldEle() {
    const [open,
        setOpen] = React.useState(false);
    const [val,
        setVal] = React.useState('');

    const handleClick = (x) => {
        setOpen(true);
        setVal(x);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };
    const [uname,
        un] = useState('');
    const [pwd,
        pd] = useState('');
    const db = firebaseapp.firestore();

    function getData() {
        console.log(pwd, uname);
        if (pwd === null || uname === null || pwd === "" || uname === "") {
            handleClick("Check Username / Password");
        } else {
            let citiesRef = db.collection('Users');
            let query = citiesRef
                .where('username', '==', uname)
                .get()
                .then(snapshot => {
                    if (snapshot.empty) {
                        console.log('No matching documents.');
                        handleClick("Check Username / Password");
                        return;
                    }
                    console.log(query);
                    snapshot.forEach(doc => {

                        if (doc.data().password === pwd) {

                            handleClick("Welcome to the Dog World");
                            login();
                            history.push('/Home');
                        } else {
                            handleClick("Check Password");
                        }
                    });
                })
                .catch(err => {
                    console.log('Error getting documents', err);
                    handleClick("Something Went Wrong");
                });
        }

    }
    function regData() {
        history.push('/Register');
    }
    const classes = useStyles();

    return (

        <Grid
            container
            alignContent="center"
            direction="rows"
            justify="center"
            alignItems="center">
            <Snackbar
                anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left'
            }}
                open={open}
                autoHideDuration={6000}
                onClose={handleClose}
                message={val}
                action={< React.Fragment > <Button color="secondary" size="small" onClick={handleClose}>
                Close
            </Button>
            </React.Fragment>}/>
            <Card className={classes.root}>
                <CardContent>
                    <CardHeader>
                        <CardMedia
                            className="App_logo"
                            image="https://i.pinimg.com/originals/11/f4/90/11f490f709630c5ac00c56930928ee19.png"
                            title="Contemplative Reptile"/>
                    </CardHeader>
                    <form className={classes.formroot} noValidate autoComplete="off">

                        <TextField
                            className={classes.TextInput}
                            id="outlined-basic"
                            label="Username"
                            type="text"
                            value={uname}
                            onChange={e => un(e.target.value)}
                            variant="outlined"/>
                        <TextField
                            className={classes.TextInput}
                            id="outlined-basic"
                            label="Password"
                            type="password"
                            value={pwd}
                            onChange={e => pd(e.target.value)}
                            variant="outlined"/>
                        <div className={classes.ButtonDiv}>
                            <Button
                                variant="outlined"
                                className={classes.ButtonEle}
                                color="primary"
                                onClick={getData}>
                                Sign In
                            </Button>

                            <Button
                                variant="outlined"
                                className={classes.ButtonEle}
                                color="primary"
                                onClick={regData}>
                                Sign Up
                            </Button>

                        </div>

                    </form>
                </CardContent>

            </Card>
        </Grid>
    );
}
export default TextFieldEle;