import React, {useState, useEffect} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import '../App.css';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Paper from '@material-ui/core/Paper';
import {logout} from '../utils';
import history from '../services/history';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles({
    root: {

        textAlign: "left",

        margin: 30,
        alignContent: "center",
        justifyContent: "center"

    },
    TextInput: {
        borderColor: "#0099d9",
        margin: 15
    },
    paper: {
        margin: 15,
        padding: 30,
        textAlign: 'center',
        color: '#0099d9'
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)'
    },
    title: {
        fontSize: 14
    },
    pos: {
        marginBottom: 12
    },
    ButtonDiv: {
        flexDirection: "columns"
    },
    ButtonEle: {
        margin: 15
    },
    tittle: {
        flexGrow: 1
    }
});

function Detail(props) {

    const {val} = props.location.state;
    const items = [];
    const [imgg,
        setImg] = useState([]);
    const classes = useStyles();
    const [isLoaded,
        setIsLoaded] = useState(false);
    const [error,
        setError] = useState(null);
    useEffect(() => {
        fetch("https://dog.ceo/api/breed/" + val + "/images/random/9")
            .then(res => res.json())
            .then((result) => {
                setIsLoaded(true);
                setImg((result.message));

            }, (error) => {
                setIsLoaded(true);
                setError(error);
                console.log("error");
            })
    }, [])
    function handleLogout() {
        logout();
        history.push("/");
    }

    if (error) {
        return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
        items.push(<CircularProgress/>);
    } else {
        for (const [index,
            value]of imgg.entries()) {
            items.push(
                <Grid key={index} item xs={12} sm={4}>
                    <Paper className={classes.paper}><img src={value} alt={val} className="App_logo"/></Paper>
                </Grid>

            );
        }
    }

    return (
        <div>
            <AppBar position="static">
                <Toolbar>

                    <Typography variant="h6" className={classes.tittle}>
                        {val}
                        Breed Images
                    </Typography>
                    <Button className="Button" onClick={handleLogout} color="inherit">Logout</Button>
                </Toolbar>

            </AppBar>
            <Grid container spacing={3}>

                {items}

            </Grid>
        </div>
    );

}
export default Detail;