import React, {useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import '../App.css';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import CardMedia from '@material-ui/core/CardMedia';
import history from '../services/history';

import Grid from '@material-ui/core/Grid';
import "firebase/firestore";
import Snackbar from '@material-ui/core/Snackbar';
import firebaseapp from '../Firebase';
const useStyles = makeStyles({
    root: {
        minWidth: 275,
        textAlign: "left",
        width: 300,
        alignContent: "center",
        justifyContent: "center"

    },
    TextInput: {
        borderColor: "#0099d9",
        margin: 15
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)'
    },
    title: {
        fontSize: 14
    },
    pos: {
        marginBottom: 12
    },
    ButtonDiv: {
        flexDirection: "columns"
    },
    ButtonEle: {
        margin: 15
    }

});

function Signup() {
    const classes = useStyles('');
    const db = firebaseapp.firestore();
    const [open,
        setOpen] = React.useState(false);
    const [val,
        setVal] = React.useState('');
    const handleClick = (x) => {
        setOpen(true);
        setVal(x);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };
    const [userName,
        updateUserName] = useState('');
    const [password,
        updatePassword] = useState('');
    const [confirmPassword,
        updateConfirmPassword] = useState('');
    const [firstName,
        updateName] = useState('');
    const [number,
        updateNumber] = useState('');
    const [mail,
        updateMail] = useState('');


    function SignInfun() {

        history.push('/');
    }
    function SignUpfun() {
        if (password === "" || userName === "" || firstName === "" || number === "" || mail === "") {
            handleClick("Check the Entries");
        } else if (password !== confirmPassword) {
            handleClick("Check the Password");
        } else {
            let data = {
                username: userName,
                password: password,
                mailID: mail,
                number: number,
                name: firstName
            };
            db
                .collection("Users")
                .add(data);
            console.log(userName, password, confirmPassword, firstName, number, mail);
            handleClick("Proceed to SignIn");
            history.push('/');
        }

    }
    return (
        <Grid
            container
            alignContent="center"
            direction="rows"
            justify="center"
            alignItems="center">
            <Snackbar
                anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left'
            }}
                open={open}
                autoHideDuration={6000}
                onClose={handleClose}
                message={val}
                action={< React.Fragment > <Button color="secondary" size="small" onClick={handleClose}>
                Close
            </Button>
             </React.Fragment>}/>
            <Card className={classes.root}>
                <CardContent>
                    <CardHeader>
                        <CardMedia
                            className="App_logo"
                            image="https://i.pinimg.com/originals/11/f4/90/11f490f709630c5ac00c56930928ee19.png"
                            title="Contemplative Reptile"/>
                    </CardHeader>
                    <form className={classes.formroot} noValidate autoComplete="off">

                        <TextField
                            className={classes.TextInput}
                            id="outlined-basic"
                            label="Name"
                            value={firstName}
                            onChange={e => updateName(e.target.value)}
                            type="text"
                            variant="outlined"/>
                        <TextField
                            className={classes.TextInput}
                            id="outlined-basic"
                            label="Username"
                            type="text"
                            value={userName}
                            onChange={e => updateUserName(e.target.value)}
                            variant="outlined"/>
                        <TextField
                            className={classes.TextInput}
                            id="outlined-basic"
                            label="Email ID"
                            type="text"
                            value={mail}
                            onChange={e => updateMail(e.target.value)}
                            variant="outlined"/>
                        <TextField
                            className={classes.TextInput}
                            id="outlined-basic"
                            label="Number"
                            type="number"
                            value={number}
                            onChange={e => updateNumber(e.target.value)}
                            variant="outlined"/>
                        <TextField
                            className={classes.TextInput}
                            id="outlined-basic"
                            label="Password"
                            type="password"
                            value={password}
                            onChange={e => updatePassword(e.target.value)}
                            variant="outlined"/>
                        <TextField
                            className={classes.TextInput}
                            id="outlined-basic"
                            label="Confirm Password"
                            type="password"
                            value={confirmPassword}
                            onChange={e => updateConfirmPassword(e.target.value)}
                            variant="outlined"/>
                        <div className={classes.ButtonDiv}>
                            <Button
                                variant="outlined"
                                className={classes.ButtonEle}
                                color="primary"
                                onClick={SignInfun}>
                                Sign In
                            </Button>
                            <Button
                                variant="outlined"
                                className={classes.ButtonEle}
                                color="primary"
                                onClick={SignUpfun}>
                                Sign Up
                            </Button>
                        </div>

                    </form>
                </CardContent>

            </Card>
        </Grid>
    );
}
export default Signup;