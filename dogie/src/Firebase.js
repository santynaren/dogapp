import * as firebase from 'firebase';


const settings = {timestampsInSnapshots: true};

const config = {
    apiKey: "AIzaSyBkOoCTHEfqX01sM40qur61DpMH3O6eYYY",
    authDomain: "dogapp-71800.firebaseapp.com",
    databaseURL: "https://dogapp-71800.firebaseio.com",
    projectId: "dogapp-71800",
    storageBucket: "dogapp-71800.appspot.com",
    messagingSenderId: "806699328868",
    appId: "1:806699328868:web:e28187737ecab51ff1c671",
    measurementId: "G-8SN62W3XD2"
};
firebase.initializeApp(config);

firebase.firestore().settings(settings);

export default firebase;