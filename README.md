# DogApp - React Web App

This Document Clearly Mentions the Work Involved in developing the Dog App . To View Project Files , Navigate to dogie Folder . Live : [Click Here](https://doggerappreact.herokuapp.com/)

**Pages**

* Login
* SignUp
* Breeds List
* Breed Specific 



**Concepts Used**

* React Material UI
* React Router
* Firebase
* Mobile First UI 
* React Hooks
* Routes
* Fetch with useEffect
* DogCEO API
* App Live on Heroku



**Main Components Created**

* TextFieldEle : Login Page '/'
* SignUp : Register Page '/Register'
* List : Breeds List with Image '/Home'
* Details : Expanded Breeds with More Images '/Breed'
* Firebase BackEnd | Dog API
* Routes 
* Local Storage 


*I have used GitLab throughout this Project .*